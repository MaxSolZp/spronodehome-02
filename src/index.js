const getInputArgumentArray = () => {
    return process.argv;
}


const writeToFile = (inputArgumentArray) => {

    const fs = require('fs');
    const fileName = 'arguments.txt';

    inputArgumentArray.forEach(function(item, i, arr) {

        try {
            fs.appendFileSync(fileName, item + "\r");
        } catch (err) {
            /* Handle the error */
        }

    });

}


writeToFile( getInputArgumentArray() );